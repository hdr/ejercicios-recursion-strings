package str;

public class EjerciciosRecursionStrings {

	// 0. Para hacer los siguientes ejercicios, primero escribir la función
	// auxiliar `public static String resto(String s)` que devuelve el
	// string `s` menos el primer caracter.
	//
	// **Esta función debe hacerse sin usar recursión.**
	public static String resto(String s) {
		String r = "";
		for (int i = 1; i < s.length(); i++) {
			r += s.charAt(i);
		}
		return r;
	}

	// 1. Escribir una función `public static int longitud(String s)` que
	// devuelve la longitud del string `s`. No vale usar s.length().
	public static int longitud(String s) {
		if (s.equals("")) {
			return 0;
		}

		return 1 + longitud(resto(s));
		// home
		// 1 + longitud("ome") → 4
		// 1 + (1 + longitud("me"))
	}

	// 2. Escribir una función recursiva llamada
	// public static void imprimirEspaciado(String s)
	// que imprima el string s con un espacio luego de cada caracter.
	// Por ejemplo, imprimirEspaciado("Juan") muestra por
	// pantalla "J u a n ".
	public static void imprimirEspaciado(String s) {
		if (s.length() == 0) {
			System.out.println();
			return;
		}

		System.out.print(s.charAt(0) + " ");
		imprimirEspaciado(resto(s));
		// s = Juan
		// "J "
		// "u a n "
	}

	// 3. Escribir una función que tome un string como parámetro y lo
	// imprima por intercalando un '∗' entre cada letra
	// (pero no al final del string).
	// Por ejemplo, si la función toma el string "hola" como argumento,
	// deberá imprimir "h∗o∗l∗a".
	public static void imprimirConAsteriscos(String s) {
		if (s.length() <= 1) {
			System.out.println(s);
			return;
		}

		System.out.print(s.charAt(0) + "*");
		imprimirConAsteriscos(resto(s));
		// h
	}

	public static void main(String[] args) {
		imprimirConAsteriscos("hola");
	}

}
